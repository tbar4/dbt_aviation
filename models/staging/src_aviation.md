{% docs src_aviation %}
This is the top level directory for aviation information
{% enddocs %}

{% docs src_aviation_metars %}
METAR is the international standard code format for hourly surface weather observations which is analogous to the SA coding currently used in the US. The acronym roughly translates from French as Aviation Routine Weather Report. 
This table captures the largest airports in the US, METARS
{% enddocs %}

{% docs src_aviation_metars_raw_text %}
The raw METAR
{% enddocs %}
{% docs src_aviation_metars_station_id %}
Station identifier; Always a four character alphanumeric( A-Z, 0-9)
{% enddocs %}
{% docs src_aviation_metars_observation_time %}
Time( in ISO8601 date/time format) this METAR was observed.
{% enddocs %}
{% docs src_aviation_metars_latitude %}
The latitude (in decimal degrees )of the station that reported this METAR
{% enddocs %}
{% docs src_aviation_metars_longitude %}
The longitude (in decimal degrees) of the station that reported this METAR 
{% enddocs %}
{% docs src_aviation_metars_temp_c %}
Air temperature 
{% enddocs %}
{% docs src_aviation_metars_dewpoint_c %}
Dewpoint temperature
{% enddocs %}
{% docs src_aviation_metars_wind_dir_degrees %}
Direction from which the wind is blowing.
0 degrees=variable wind direction. 
{% enddocs %}
{% docs src_aviation_metars_wind_speed_kt %}
Wind speed; 0 degree wdir and 0 wspd = calm winds
{% enddocs %}
{% docs src_aviation_metars_wind_gust_kt %}
Wind gust
{% enddocs %}
{% docs src_aviation_metars_visibility_statute_mi %}
Horizontal visibility
{% enddocs %}
{% docs src_aviation_metars_altim_in_hg %}
Altimeter
{% enddocs %}
{% docs src_aviation_metars_sea_level_pressure_mb %}
Sea-level pressure
{% enddocs %}
{% docs src_aviation_metars_quality_control_flags %}
Quality control flags (see below) provide useful information about the METAR station(s) that provide the data. 

|Reported Value Name| Corresponding description |
|---------|-----------|
| corrected | Corrected |
| auto | Fully automated |
| auto_station | Indicates that the automated station type is one of the following: A01, A01A, A02, A02A, AOA, AWOS - NOTE: The type of station is not returned. This simply indicates that this station is one of the six stations enumerated above.|
| maintenance_indicator | Maintenance check indicator - maintenance is needed |
| no_signal | No signal |
| lightning_sensor_off | The lightning detection sensor is not operating- thunderstorm information is not available.|
| freezing_rain_sensor_off | The freezing rain sensor is not operating |
| present_weather_sensor_off | The present weather sensor is not operating |

{% enddocs %}
{% docs src_aviation_metars_wx_string %}
[wx_string descriptions](https://aviationweather.gov/docs/metar/wxSymbols_anno2.pdf)
{% enddocs %}
{% docs src_aviation_metars_sky_cover %}
sky_cover - up to four levels of sky cover and base can be reported under the sky_conditions field; OVX present when vert_vis_ft is reported.
Allowed values: SKC|CLR|CAVOK|FEW|SCT|BKN|OVC|OVX 	string 	
cloud_base_ft_agl - height of cloud base in feet AGL. Up to four levels can be reported under the sky_conditions field. A value exists when the corresponding sky_cover='FEW','SCT','BKN', 'OVC' 
{% enddocs %}
{% docs src_aviation_metars_flight_category %}
Flight category of this METAR. Values: VFR|MVFR|IFR|LIFR
See http://www.aviationweather.gov/metar/help?page=plot#fltcat

NOTE: cloud_base_ft_agl needs to be specified to output flight_category. 
{% enddocs %}
{% docs src_aviation_metars_three_hr_pressure_tendency_mb %}
Pressure change in the past 3 hours
{% enddocs %}
{% docs src_aviation_metars_maxT_c %}
Maximum air temperature from the past 6 hours
{% enddocs %}
{% docs src_aviation_metars_minT_c %}
Minimum air temperature from the past 6 hours
{% enddocs %}
{% docs src_aviation_metars_maxT24hr_c %}
Maximum air temperature from the past 24 hours
{% enddocs %}
{% docs src_aviation_metars_minT24hr_c %}
Minimum air temperature from the past 24 hours 
{% enddocs %}
{% docs src_aviation_metars_precip_in %}
Liquid precipitation since the last regular METAR 
{% enddocs %}
{% docs src_aviation_metars_pcp3hr_in %}
Liquid precipitation from the past 3 hours. 0.0005 in = trace precipitation 
{% enddocs %}
{% docs src_aviation_metars_pcp6hr_in %}
Liquid precipitation from the past 6 hours. 0.0005 in = trace precipitation 
{% enddocs %}
{% docs src_aviation_metars_pcp24hr_in %}
Liquid precipitation from the past 24 hours. 0.0005 in = trace precipitation 
{% enddocs %}
{% docs src_aviation_metars_snow_in %}
Snow depth on the ground
{% enddocs %}
{% docs src_aviation_metars_vert_vis_ft %}
Vertical Visibility
{% enddocs %}
{% docs src_aviation_metars_metar_type %}
METAR or SPECI 
{% enddocs %}
{% docs src_aviation_metars_elevation_m %}
The elevation of the station that reported this METAR 
{% enddocs %}
